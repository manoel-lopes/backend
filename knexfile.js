module.exports = {
  development: {
    client: 'mysql2',
    connection: {
      database: 'challenge_backend',
      user: 'root',
      password: 'root',
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      directory: './src/db/migrations',
      tableName: 'knex_migrations',
    },
  },
}
