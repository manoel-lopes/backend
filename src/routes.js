const { router, get, post, put, del } = require('microrouter')
const UserController = require('./app/controllers/user-controller')

const userController = new UserController()

const routes = router(
  post('/users', userController.create),
  get('/users', userController.index),
  get('/users/:id', userController.show),
  put('/users/:id', userController.update),
  del('/users/:id', userController.delete)
)

module.exports = routes
