const guid = require('objection-guid')()
const { Model } = require('objection')

class User extends guid(Model) {
  static get tableName() {
    return 'user'
  }
}

module.exports = User
