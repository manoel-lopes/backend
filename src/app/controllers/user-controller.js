const { send, json } = require('micro')
const joi = require('joi')
const { hash } = require('bcryptjs')

const User = require('../models/user')
const createAppError = require('../errors/create-app-error')

class UserController {
  async create(req, res) {
    const emailTest = /[\w.]+@[\w]+\.[a-z]+(\.[a-z]+)?$/i

    const schema = joi.object({
      name: joi.string().required(),
      email: joi.string().required().pattern(new RegExp(emailTest)),
      password: joi.string().required(),
    })

    const { name, email, password } = await json(req)

    const { error } = schema.validate(
      { name, email, password },
      { abortEarly: false }
    )

    if (error) {
      const { message } = error.details[0]

      const isValidEmailError = message.includes(
        'fails to match the required pattern'
      )

      const errorMessage = isValidEmailError ? 'invalid email' : message
      const statusCode = isValidEmailError ? 400 : 404

      throw createAppError({ statusCode, message: errorMessage })
    }

    const hashPassword = await hash(password, 10)

    const user = await User.query().insert({
      name,
      email,
      password: hashPassword,
    })

    send(res, 201, user)
  }

  async index(_, res) {
    const users = await User.query().select()

    send(res, 200, users)
  }

  async show(req, res) {
    const { id } = req.params

    const user = await User.query().findById(id)

    if (!user)
      throw createAppError({ statusCode: 404, message: 'user not found' })

    send(res, 200, user)
  }

  async update(req, res) {
    const { id } = req.params

    const user = await json(req)

    if (user.password) user.password = encryptPassword(user.password)

    const updateUsers = await User.query()
      .findById(id)
      .patch({ ...user })

    if (!updateUsers)
      throw createAppError({ statusCode: 404, message: 'user not found' })

    const newUser = await User.query().findById(id)

    send(res, 200, newUser)
  }

  async delete(req, res) {
    const { id } = req.params

    const deletedUsers = await User.query().deleteById(id)

    if (!deletedUsers)
      throw createAppError({ statusCode: 404, message: 'user not found' })

    send(res, 200)
  }
}

module.exports = UserController
