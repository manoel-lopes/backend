const { createError } = require('micro')

const createAppError = error => {
  return createError(error.statusCode, error.message)
}

module.exports = createAppError
