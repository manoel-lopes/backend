const createConnection = require('./db')
const routes = require('./routes')

createConnection()

module.exports = routes
